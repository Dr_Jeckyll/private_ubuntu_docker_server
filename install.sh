#!/bin/bash 
# Root directory for docker stuff
sudo mkdir -p /var/docker/registry
sudo cp config.yml /var/docker/registry

# Location for storing images
sudo mkdir /var/docker/registry/lib

# Proxy directory
sudo mkdir -p /var/docker/nginx-proxy/vhost.d 
sudo mkdir /var/docker/nginx-proxy/certs.d 
sudo mkdir /var/docker/nginx-proxy/htpasswd

# Generate ssl certs
sudo openssl req -x509 -nodes -days 3650 -newkey rsa:2048 \
        -subj "/C=CA/ST=Saskatchewan/L=Regina/O=FCC/OU=EDnA/CN=example.dockerhub.fcc.ca"\
        -keyout proxy-selfsigned.key \
        -out proxy-selfsigned.crt

sudo openssl req -x509 -nodes -days 3650 -newkey rsa:2048 \
        -subj "/C=CA/ST=Saskatchewan/L=Regina/O=FCC/OU=EDnA/CN=example.fcc.ca"\
        -keyout registry-selfsigned.key \
        -out registry-selfsigned.crt

# Move certs to the certs.d directory
sudo mv registry-selfsigned.key /var/docker/nginx-proxy/certs.d/example.dockerhub.fcc.ca.key 
sudo mv registry-selfsigned.crt /var/docker/nginx-proxy/certs.d/example.dockerhub.fcc.ca.crt

sudo mv proxy-selfsigned.key /var/docker/nginx-proxy/certs.d/example.fcc.ca.key 
sudo mv proxy-selfsigned.crt /var/docker/nginx-proxy/certs.d/example.fcc.ca.crt

# Generate htpasswd for ben
echo "INFO: Generating htpasswd for ben, create password: "
sudo htpasswd -c /var/docker/nginx-proxy/htpasswd/example.dockerhub.fcc.ca ben

# Move the registry.example.com to vhost.d
sudo cp myregistry.example.com /var/docker/nginx-proxy/vhost.d/example.dockerhub.fcc.ca 
sudo cp docker-registry.conf /var/docker/nginx-proxy/vhost.d/

# Copy the config
sudo cp Registry/config.yml /var/docker/registry/config.yml
