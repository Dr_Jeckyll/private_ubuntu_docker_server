#!/bin/bash 

# Update and grab dependencies
sudo apt update
sudo apt install apt-transport-https \
                 ca-certificates \ 
                 curl \
                 software-properties-common \
                 git \
                 apache2-utils

# Get the gpg key from docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
# Add the docker repo
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update
apt-cache policy docker-ce 

# Install docker
sudo apt install docker-ce

# Check service is running
sudo systemctl status docker 