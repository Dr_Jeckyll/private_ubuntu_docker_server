#!/bin/bash 

# Root directory for docker stuff
sudo mkdir -p /var/docker/registry
sudo cp config.yml /var/docker/registry

# Location for storing images
sudo mkdir /var/docker/registry/lib

# Proxy directory
sudo mkdir -p /var/docker/nginx-proxy/vhost.d 
sudo mkdir /var/docker/nginx-proxy/certs.d 
sudo mkdir /var/docker/nginx-proxy/htpasswd

# Generate ssl certs
sudo openssl req -x509 -nodes -days 3650 -newkey rsa:2048 \
        -subj "/C=CA/ST=Saskatchewan/L=Regina/O=FCC/OU=EDnA/CN=example.dockerhub.com"\
        -keyout proxy-selfsigned.key \
        -out proxy-selfsigned.crt

sudo openssl req -x509 -nodes -days 3650 -newkey rsa:2048 \
        -subj "/C=CA/ST=Saskatchewan/L=Regina/O=FCC/OU=EDnA/CN=example.com"\
        -keyout registry-selfsigned.key \
        -out registry-selfsigned.crt

# Move certs to the certs.d directory
sudo mv registry-selfsigned.key /var/docker/nginx-proxy/certs.d/example.dockerhub.com.key 
sudo mv registry-selfsigned.crt /var/docker/nginx-proxy/certs.d/example.dockerhub.com.crt

sudo mv proxy-selfsigned.key /var/docker/nginx-proxy/certs.d/example.com.key 
sudo mv proxy-selfsigned.crt /var/docker/nginx-proxy/certs.d/example.com.crt

# Generate htpasswd for ben
echo "INFO: Generating htpasswd for ben, create password: "
sudo htpasswd /var/docker/nginx-proxy/htpasswd/example.dockerhub.com ben

# Move the registry.example.com to vhost.d
sudo mv registry.example.com /var/docker/nginx-proxy/vhost.d/example.dockerhub.com 
sudo mv docker-registry.conf /var/docker/nginx-proxy/vhost.d/

